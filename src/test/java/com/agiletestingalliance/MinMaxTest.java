package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MinMaxTest {

    @Test
    public void testFindBiggerNumber() {
        MinMax minMax = new MinMax();
        assertEquals(10, minMax.findBiggerNumber(5, 10));
        assertEquals(5, minMax.findBiggerNumber(5, 3));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("test", minMax.bar("test"));
        assertNull(minMax.bar(null));
        assertNull(minMax.bar(""));
    }
}