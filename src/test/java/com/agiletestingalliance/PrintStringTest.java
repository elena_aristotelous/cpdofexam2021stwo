package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PrintStringTest {

    @Test
    public void testGstr() {
        PrintString printString = new PrintString("test");
        assertEquals("test", printString.gstr());
    }
}
